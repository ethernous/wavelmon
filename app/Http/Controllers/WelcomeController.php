<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Lokasi;

use App\Models\Monitoring;

use App\Models\Monitoringdata;

use App\Models\Online;

use App\Models\Offline;

use Carbon\Carbon;

class WelcomeController extends Controller
{
    public function index()
    {
        $lokasi = Lokasi::count();
        $monitoring = Monitoring::count();
        $online = Online::count();
        $offline = Offline::count();
        $tabeloff = Offline::all();
        $tabelon = Online::all();

        $widget = [
            'lokasi' => $lokasi,
            'monitoring' => $monitoring,
            'online' => $online,
            'offline' => $offline,
            'tabeloff' => $tabeloff,
            'tabelon' => $tabelon
            //...
        ];

        return view('welcome', compact('widget'));
    }
}
