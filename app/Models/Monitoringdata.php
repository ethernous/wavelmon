<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Monitoringdata extends Model
{
    use HasFactory;
    protected $table = 'monitoringdata';
    protected $guarded = [];
    protected  $primaryKey = 'namadevice';
    public $incrementing = 'false';
    public $keyType = 'string';

}
