<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Models\Monitoring;

use App\Models\Monitoringdata;

use App\Models\Online;

use App\Models\Offline;

use Carbon\Carbon;

class MonitoringController extends Controller
{
    public function index($id)
    {
        DB::table('offline')->truncate();
        DB::table('online')->truncate();
        $monitoring = Monitoring::find($id);
        $waktu = $monitoring->updated_at;
        $akhir = new Carbon('Asia/Jakarta');
        $akhira = Carbon::now();
        $interval = $akhira->diffInMinutes($waktu);
        if ($interval > 5){
            DB::insert('insert into offline (id_offline, namamonitoring, alamat, latitude, longitude, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [NULL, $monitoring->namamonitoring, $monitoring->alamat, $monitoring->latitude, $monitoring->longitude, $akhira, $akhira]);
            DB::update('update monitoring set status = ? where namamonitoring=?', ['Offline', $monitoring->namamonitoring]);
        } else {
            DB::insert('insert into online (id_online, namamonitoring, alamat, latitude, longitude, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [NULL, $monitoring->namamonitoring, $monitoring->alamat, $monitoring->latitude, $monitoring->longitude, $akhira, $akhira]);
            DB::update('update monitoring set status = ? where namamonitoring=?', ['Online', $monitoring->namamonitoring]);
        }
        $monitoringdata = Monitoringdata::where('namadevice', '=', $id)->orderBy('id_data', 'desc')->first();
        $chart = Monitoringdata::where('namadevice', '=', $id)->orderBy('created_at', 'desc')->get();
        return view('/detail', ['monitoring' => $monitoring, 'monitoringdata' => $monitoringdata, 'chart' => $chart]);
    }
}
