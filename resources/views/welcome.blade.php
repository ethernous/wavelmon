@extends('layouts.app')
@section('main-content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Beranda</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Beranda</li>
              </ol>
            </nav>
          </div>
        </div>
        <!-- Card stats -->
        <div class="row">
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Jumlah Lokasi</h5>
                    <span class="h2 font-weight-bold mb-0">{{ $widget['lokasi'] }}</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                      <i class="ni ni-map-big"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Jumlah Perangkat</h5>
                    <span class="h2 font-weight-bold mb-0">{{ $widget['monitoring'] }}</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-chart-pie-35"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Perangkat online</h5>
                    <span class="h2 font-weight-bold mb-0"><a href="#" data-toggle="modal" data-target="#modal-default">{{ $widget['online'] }}</a></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                      <i class="ni ni-check-bold"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Perangkat offline</h5>
                    <span class="h2 font-weight-bold mb-0"><a href="#" data-toggle="modal" data-target="#modal-error">{{ $widget['offline'] }}</a></span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-fat-remove"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Page content -->
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card border-0">
            <div id="map" class="map-canvas"></div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <footer class="footer pt-0">
      <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6">
          <div class="copyright text-center  text-lg-left  text-muted">
            &copy; 2021 <a href="https://www.probisdigital.id" class="font-weight-bold ml-1" target="_blank">Mitratel - Digital Bisnis</a>
          </div>
        </div>
        <div class="col-lg-6">
          <ul class="nav nav-footer justify-content-center justify-content-lg-end">
            <li class="nav-item">
              <a href="http://www.mitratel.co.id" class="nav-link" target="_blank">Mitratel</a>
            </li>
            <li class="nav-item">
              <a href="https://www.probisdigital.id" class="nav-link" target="_blank">Digital Bisnis</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  </div>
</div>
<div class="modal fade" id="modal-default" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-default">Daftar Perangkat Online :</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <td>Nama Perangkat</td>
                    <td>Lokasi</td>
                    <td>Aksi</td>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($widget['tabelon'] as $on)
                      <tr>
                        <td>{{ $on->namamonitoring }}</td>
                        <td>{{ $on->alamat }}</td>
                        <td>
                        <a href="/detail/{{ $on->namamonitoring }}" data-toggle="tooltip" data-placement="top" title="Lihat Detail"> <i class="fas fa-bars"></i></a>
                        <a href="https://www.google.com/maps/search/{{ $on->latitude }},{{ $on->longitude }}" target="blank" data-toggle="tooltip" data-placement="top" title="Ke Lokasi"><i class="fas fa-location-arrow"></i></a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-error" tabindex="-1" role="dialog" aria-labelledby="modal-error" aria-hidden="true">
  <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
      <div class="modal-content">

          <div class="modal-header">
              <h6 class="modal-title" id="modal-title-error">Daftar Perangkat Offline :</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
              </button>
          </div>

          <div class="modal-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <td>Nama Perangkat</td>
                  <td>Lokasi</td>
                  <td>Aksi</td>
                </tr>
              </thead>
              <tbody>
                @foreach ($widget['tabeloff'] as $on)
                    <tr>
                      <td>{{ $on->namamonitoring }}</td>
                      <td>{{ $on->alamat }}</td>
                      <td>
                      <a href="/detail/{{ $on->namamonitoring }}" data-toggle="tooltip" data-placement="top" title="Lihat Detail"> <i class="fas fa-bars"></i></a>
                      <a href="https://www.google.com/maps/search/{{ $on->latitude }},{{ $on->longitude }}" target="blank" data-toggle="tooltip" data-placement="top" title="Ke Lokasi"><i class="fas fa-location-arrow"></i></a>
                      </td>
                    </tr>
                @endforeach
              </tbody>
            </table>

          </div>
      </div>
  </div>
</div>
@endsection
@push('scripts')
<script>
    var locations = [
    		@foreach ($allUser2 as $siteatt)
             	["{{$siteatt['namamonitoring']}}", {{$siteatt['latitude']}},
             	{{$siteatt['longitude']}}],
       		 @endforeach
        ];

    var map = L.map('map').setView([-2.2406396093827206,  121.46484375000001], 5);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>',
        id: 'mapbox.streets'
        }).addTo(map);

    for (var i = 0; i < locations.length; i++) {
    marker = new L.marker([locations[i][1], locations[i][2]])
    .bindPopup(locations[i][0])
    .addTo(map);
    }
  </script>
@endpush