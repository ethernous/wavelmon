<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonitoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring', function (Blueprint $table) {
            $table->id('id_monitoring');
            $table->string('namamonitoring');
            $table->string('namasite');
            $table->string('alamat');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('linktinggi');
            $table->string('linktegang');
            $table->string('linkarus');
            $table->string('status');
            $table->string('siaga1');
            $table->string('siaga2');
            $table->string('siaga3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring');
    }
}
