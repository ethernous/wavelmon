<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\WelcomeController@index')->name('welcome');

Route::get('/lokasi/{id}', 'App\Http\Controllers\LokasiController@index')->name('lokasi');

Route::get('/detail/{id}', 'App\Http\Controllers\MonitoringController@index')->name('monitoring');

Route::get('/search', 'App\Http\Controllers\SearchController@index');

Route::get('/tentang', 'App\Http\Controllers\TentangController@index')->name('tentang');
Route::post('/tentang/send', 'App\Http\Controllers\TentangController@send')->name('tentang.send');