<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Models\Lokasi;

use App\Models\Monitoring;

use Carbon\Carbon;

class LokasiController extends Controller
{
    public function index($id)
    {
        DB::table('offline')->truncate();
        DB::table('online')->truncate();
        $monitoring = Monitoring::all();
        foreach ($monitoring as $mo){
            $waktu = $mo->updated_at;
            $akhir = new Carbon('Asia/Jakarta');
            $akhira = Carbon::now();
            $interval = $akhira->diffInMinutes($waktu);
            if ($interval > 5){
                DB::insert('insert into offline (id_offline, namamonitoring, alamat, latitude, longitude, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [NULL, $mo->namamonitoring, $mo->alamat, $mo->latitude, $mo->longitude, $akhira, $akhira]);
                DB::update('update monitoring set status = ? where namamonitoring=?', ['Offline', $mo->namamonitoring]);
            } else {
                DB::insert('insert into online (id_online, namamonitoring, alamat, latitude, longitude, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [NULL, $mo->namamonitoring, $mo->alamat, $mo->latitude, $mo->longitude, $akhira, $akhira]);
                DB::update('update monitoring set status = ? where namamonitoring=?', ['Online', $mo->namamonitoring]);
            }
        }
        $lokasi = Lokasi::find($id);
        $monitoring = Monitoring::where('alamat', $id)->get();
        return view('/lokasi', ['lokasi' => $lokasi, 'monitoring' => $monitoring]);
    }
}
