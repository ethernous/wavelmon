@extends('layouts.app')
@section('main-content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">{{ $monitoring->namasite }}</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="/lokasi/{{ $monitoring->alamat}}">{{ $monitoring->alamat}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $monitoring->namasite }}</li>
              </ol>
            </nav>
          </div>
          <div class="col-lg-6 col-5 text-right">
            @if ($monitoring->status == 'Online')
            <a href="#" class="btn btn-sm btn-success">{{ $monitoring->status }}</a>
            @else
            <a href="#" class="btn btn-sm btn-danger">{{ $monitoring->status }}</a>
            @endif
            
          </div>
        </div>
        <!-- Card stats -->
        <div class="row">
          <div class="col-xl-9 col-md-9">
              <div class="card border-0">
                  <div class="map-canvas">
                    <iframe src="{{ $monitoring->linktinggi }}" width="100%" height="100%" frameborder="0"></iframe>
                  </div>
              </div>
          </div>
          <div class="col-xl-3 col-md-3">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col-auto">
                    <div class="icon icon-shape shadow">
                      <i class="fas fa-info"></i>
                    </div>
                  </div>
                  <div class="col-auto">
                    <h5 class="card-title text-uppercase text-muted mb-0">Petunjuk</h5>
                    <span class="h2 font-weight-bold mb-0">Klasifikasi Tinggi</span>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                      <i class="fas fa-exclamation-triangle"></i>
                    </div>
                  </div>
                  <div class="col-auto">
                    <h5 class="card-title text-uppercase text-muted mb-0">Siaga III</h5>
                    <span class="h2 font-weight-bold mb-0">{{ $monitoring->siaga3 }}</span>
                  </div>
                </div>
                <hr>
                  <div class="row">
                    <div class="col-auto">
                      <div class="icon icon-shape bg-orange text-white rounded-circle shadow">
                        <i class="fas fa-exclamation-triangle"></i>
                      </div>
                    </div>
                    <div class="col-auto">
                      <h5 class="card-title text-uppercase text-muted mb-0">Siaga II</h5>
                      <span class="h2 font-weight-bold mb-0">{{ $monitoring->siaga2 }}</span>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                      <div class="col-auto">
                        <div class="icon icon-shape bg-red text-white rounded-circle shadow">
                          <i class="fas fa-exclamation-triangle"></i>
                        </div>
                      </div>
                      <div class="col-auto">
                        <h5 class="card-title text-uppercase text-muted mb-0">Siaga I</h5>
                        <span class="h2 font-weight-bold mb-0">{{ $monitoring->siaga1 }}</span>
                      </div>
                  </div>
                  <hr>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Tinggi Muka Air</h5>
                    <span class="h2 font-weight-bold mb-0">{{ $monitoringdata->level_sensor }} M</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                      <i class="ni ni-align-left-2"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Tegangan Perangkat</h5>
                    <span class="h2 font-weight-bold mb-0">{{ $monitoringdata->power_voltage }} V</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                      <i class="ni ni-sound-wave"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Arus Perangkat</h5>
                    <span class="h2 font-weight-bold mb-0">{{ $monitoringdata->power_current }} A</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                      <i class="ni ni-sound-wave"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6">
            <div class="card card-stats">
              <!-- Card body -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5 class="card-title text-uppercase text-muted mb-0">Terupdate pada:</h5>
                    <span class="h2 font-weight-bold mb-0">{{ $monitoringdata->created_at }}</span>
                  </div>
                  <div class="col-auto">
                    <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                      <i class="ni ni-time-alarm"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Page content -->
  <div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-6">
          <div class="card border-0">
              <div class="map-canvas">
                  <iframe src="{{ $monitoring->linktegang }}" width="100%" height="100%" frameborder="0"></iframe>
              </div>
          </div>
        </div>
        <div class="col-xl-6">
          <div class="card border-0">
              <div class="map-canvas">
                  <iframe src="{{ $monitoring->linkarus }}" width="100%" height="100%" frameborder="0"></iframe>
              </div>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="card border-0">
            <div id="map" class="map-canvas"></div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <footer class="footer pt-0">
      <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6">
          <div class="copyright text-center  text-lg-left  text-muted">
            &copy; 2021 <a href="https://www.probisdigital.id" class="font-weight-bold ml-1" target="_blank">Mitratel - Digital Bisnis</a>
          </div>
        </div>
        <div class="col-lg-6">
          <ul class="nav nav-footer justify-content-center justify-content-lg-end">
            <li class="nav-item">
              <a href="http://www.mitratel.co.id" class="nav-link" target="_blank">Mitratel</a>
            </li>
            <li class="nav-item">
              <a href="https://www.probisdigital.id" class="nav-link" target="_blank">Digital Bisnis</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  </div>
</div>
@endsection
@push('scripts')
<script>
    var map = L.map('map').setView([{{ $monitoring->latitude }},  {{ $monitoring->longitude }}], 18);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>',
        id: 'mapbox.streets'
        }).addTo(map);

    marker = new L.marker([{{ $monitoring->latitude }},  {{ $monitoring->longitude }}])
    .bindPopup("{{$monitoring->namamonitoring}}")
    .addTo(map);
  </script>
@endpush