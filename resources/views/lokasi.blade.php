@extends('layouts.app')
@section('main-content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Lokasi</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Lokasi</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Page content -->
  <div class="container-fluid mt--6">
    <div class="row">
      <div class="col">
        <div class="card">
          <!-- Card header -->
          <div class="card-header border-0">
            <h3 class="mb-0">Daftar lokasi pemantauan di {{ $lokasi->namalokasi }}</h3>
          </div>
          <!-- Light table -->
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col" class="sort" data-sort="name">Nama Perangkat</th>
                  <th scope="col" class="sort" data-sort="name">Nama Site</th>
                  <th scope="col" class="sort" data-sort="name">Lokasi</th>
                  <th scope="col" class="sort" data-sort="name">Latitude</th>
                  <th scope="col" class="sort" data-sort="name">Longitude</th>
                  <th scope="col" class="sort" data-sort="status">Status</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody class="list">
                @foreach ($monitoring as $m)
                <tr>
                  <th scope="row">
                    <div class="media align-items-center">
                      <a href="#" class="avatar rounded-circle mr-3">
                        <img alt="Image placeholder" src="{{ asset('img/theme/river.png') }}">
                      </a>
                      <div class="media-body">
                        <span class="name mb-0 text-sm">{{ $m->namamonitoring }}</span>
                      </div>
                    </div>
                  </th>
                  <td class="budget">
                    {{ $m->namasite }}
                  </td>
                  <td class="budget">
                    {{ $m->alamat }}
                  </td>
                  <td class="budget">
                    {{ $m->latitude }}
                  </td>
                  <td class="budget">
                    {{ $m->longitude }}
                  </td>
                  <td>
                    <span class="badge badge-dot mr-4">
                      @if ($m->status == 'Online')
                      <i class="bg-success"></i>
                      <span class="status">{{ $m->status }}</span>
                      @else
                      <i class="bg-warning"></i>
                      <span class="status">{{ $m->status }}</span>
                      @endif
                    </span>
                  </td>
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="/detail/{{ $m->namamonitoring }}"><i class="fas fa-bars"></i> Lihat Detail</a>
                        <a class="dropdown-item" href="https://www.google.com/maps/search/{{ $m->latitude }},{{ $m->longitude }}" target="blank"><i class="fas fa-location-arrow"></i> Ke Lokasi</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <footer class="footer pt-0">
      <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6">
          <div class="copyright text-center  text-lg-left  text-muted">
            &copy; 2021 <a href="https://www.probisdigital.id" class="font-weight-bold ml-1" target="_blank">Mitratel - Digital Bisnis</a>
          </div>
        </div>
        <div class="col-lg-6">
          <ul class="nav nav-footer justify-content-center justify-content-lg-end">
            <li class="nav-item">
              <a href="http://www.mitratel.co.id" class="nav-link" target="_blank">Mitratel</a>
            </li>
            <li class="nav-item">
              <a href="https://www.probisdigital.id" class="nav-link" target="_blank">Digital Bisnis</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  </div>
</div>
@endsection