@extends('layouts.app')
@section('main-content')

    <div class="header pb-6 d-flex align-items-center" style="min-height: 500px; background-image: url({{ asset('img/theme/background.jpg') }}); background-size: cover; background-position: center top;">
      
      <span class="mask bg-gradient-primary opacity-8"></span>

      <div class="container-fluid d-flex align-items-center">
        <div class="row">
          <div class="col-lg-7 col-md-10">
            <h1 class="display-2 text-white">Halo Pengunjung</h1>
            <p class="text-white mt-0 mb-5">Aplikasi ini dibuat untuk memenuhi kebutuhan use case monitoring ketinggian muka air milik PT. Dayamitra Telekomunikasi (Mitratel). </p>
          </div>
        </div>
      </div>
    </div>
   
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">
            <img src="{{ asset('img/theme/theme.jpg') }}" alt="Image placeholder" class="card-img-top">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="{{ asset('img/theme/mitratel.jpg') }}" class="rounded-circle">
                  </a>
                </div>
              </div>
            </div>
            <hr>
            <div class="card-body pt-0">
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center">
                    <div>
                      <span class="heading"><a href="https://id.linkedin.com/company/pt.-dayamitra-telekomunikasi-mitratel-"><i class="fab fa-linkedin"></i></a></span>
                      <span class="description">LinkedIn</span>
                    </div>
                    <div>
                      <span class="heading"><a href="https://www.instagram.com/mitratel"><i class="fab fa-instagram"></i></a></span>
                      <span class="description">Instagram</span>
                    </div>
                    <div>
                      <span class="heading"><a href="http://www.mitratel.co.id"><i class="fas fa-globe"></i></a></span>
                      <span class="description">Website</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-center">
                <h5 class="h3">
                  PT. Dayamitra Telekomunikasi
                </h5>
                <div class="h5 font-weight-300">
                  <i class="ni location_pin mr-2"></i>Telkom Landmark Tower Lantai 25 - 27
                </div>
                <div class="h5 mt-4">
                  Jl. Gatot Subroto No.Kav. 52, RT.6/RW.1, Kuningan Barat, Mampang Prapatan
                </div>
                <div>
                  Jakarta Selatan - Indonesia
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Hubungi Kami </h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="{{ url('tentang/send') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h6 class="heading-small text-muted mb-4">Informasi Pengunjung</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label">Nama Lengkap</label>
                        <input type="text" class="form-control" placeholder="Dayamitra Telekomunikasi" name="name">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label">Alamat Email</label>
                        <input type="email" class="form-control" placeholder="dayamitra@mitratel.com" name="email">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Pesan Anda</h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Tulis Pesan Anda Disini</label>
                    <textarea rows="4" class="form-control" placeholder="Saya ingin mengetahui lebih detail tentang produk Digital Bisnis Mitratel" name="pesan"></textarea>
                  </div>
                  <div class="form-group">
                    <input type="submit" name="send" class="btn btn-danger" value="Kirim Pesan" />
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; 2021 <a href="https://www.probisdigital.id" class="font-weight-bold ml-1" target="_blank">Mitratel - Digital Bisnis</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="http://www.mitratel.co.id" class="nav-link" target="_blank">Mitratel</a>
              </li>
              <li class="nav-item">
                <a href="https://www.probisdigital.id" class="nav-link" target="_blank">Digital Bisnis</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
    <div class="col-md-4">
        <div class="modal fade" id="successmodal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
            <div class="modal-dialog modal-success modal-dialog-centered modal-" role="document">
            <div class="modal-content bg-gradient-success">
                <div class="modal-header">
                <h6 class="modal-title" id="modal-title-notification">Terimakasih telah menghubungi kami</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                </div>
                <div class="modal-body">
                <div class="py-3 text-center">
                    <i class="ni ni-check-bold ni-3x"></i>
                    <h4 class="heading mt-4">Pesan anda telah terkirim</h4>
                    <p>Kami akan segera menghubungi anda</p>
                </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="modal fade" id="errormodal" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
            <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
            <div class="modal-content bg-gradient-danger">
                <div class="modal-header">
                <h6 class="modal-title" id="modal-title-notification">Mohon perhatian anda</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                </div>
                <div class="modal-body">
                <div class="py-3 text-center">
                    <i class="ni ni-bell-55 ni-3x"></i>
                    <h4 class="heading mt-4">Berikut adalah error yang terjadi</h4>
                    @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Tutup</button>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@if ($message = Session::get('success'))
<script type="text/javascript">
 $(document).ready(function() {
   $('#successmodal').modal();
 });
</script>
@endif
@if (count($errors) > 0)
<script type="text/javascript">
$(document).ready(function() {
    $('#errormodal').modal();
});
</script>
@endif
@endpush