<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonitoringdata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoringdata', function (Blueprint $table) {
            $table->id('id_data');
            $table->string('namadevice');
            $table->float('level_sensor', 8, 4);
            $table->float('power_voltage', 8, 2);
            $table->float('power_current', 8, 2);
            $table->string('latitude');
            $table->string('longitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoringdata');
    }
}
