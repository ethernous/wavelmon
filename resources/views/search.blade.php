@extends('layouts.app')
@section('main-content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
      <div class="header-body">
        <div class="row align-items-center py-4">
          <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Hasil Pencarian</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="{{ route('welcome') }}"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Hasil Pencarian</li>
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-12 col-md-12">
      <div class="card card-stats">
        <!-- Card body -->
        <div class="card-body">
            @forelse ($lokasi as $lok)
            <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Berikut adalah hasil pencarian kami :</h5>
                  <span class="h2 font-weight-bold mb-0"><a href="/lokasi/{{ $lok->namalokasi }}"> {{ $lok->namalokasi }}</a></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="fas fa-search"></i>
                  </div>
                </div>
            </div>
            @empty
            <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Maaf, hasil pencarian tidak ditemukan</h5>
                  <span class="h2 font-weight-bold mb-0"><a href="{{ route('welcome') }}"><button class="btn btn-secondary"> Kembali </button></a></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                    <i class="fas fa-search"></i>
                  </div>
                </div>
            </div>
                
            @endforelse
          
        </div>
      </div>
    </div>
  <!-- Page content -->
  <div class="container-fluid mt--6">
      <hr>
      <hr>
    <!-- Footer -->
    <footer class="footer pt-0">
      <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6">
          <div class="copyright text-center  text-lg-left  text-muted">
            &copy; 2021 <a href="https://www.probisdigital.id" class="font-weight-bold ml-1" target="_blank">Mitratel - Digital Bisnis</a>
          </div>
        </div>
        <div class="col-lg-6">
          <ul class="nav nav-footer justify-content-center justify-content-lg-end">
            <li class="nav-item">
              <a href="http://www.mitratel.co.id" class="nav-link" target="_blank">Mitratel</a>
            </li>
            <li class="nav-item">
              <a href="https://www.probisdigital.id" class="nav-link" target="_blank">Digital Bisnis</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  </div>
</div>
@endsection