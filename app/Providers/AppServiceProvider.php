<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\DB;

use App\Models\Lokasi;

use App\Models\Monitoring;

use Carbon\Carbon;

use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::composer('*', function($view)
        {
            DB::table('offline')->truncate();
            DB::table('online')->truncate();
            $monitoring = Monitoring::all();
            foreach ($monitoring as $mo){
                $waktu = $mo->updated_at;
                $akhir = new Carbon('Asia/Jakarta');
                $akhira = Carbon::now();
                $interval = $akhira->diffInMinutes($waktu);
                if ($interval > 5){
                    DB::insert('insert into offline (id_offline, namamonitoring, alamat, latitude, longitude, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [NULL, $mo->namamonitoring, $mo->alamat, $mo->latitude, $mo->longitude, $akhira, $akhira]);
                    DB::update('update monitoring set status = ? where namamonitoring=?', ['Offline', $mo->namamonitoring]);
                } else {
                    DB::insert('insert into online (id_online, namamonitoring, alamat, latitude, longitude, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?)', [NULL, $mo->namamonitoring, $mo->alamat, $mo->latitude, $mo->longitude, $akhira, $akhira]);
                    DB::update('update monitoring set status = ? where namamonitoring=?', ['Online', $mo->namamonitoring]);
                }
            }
            $allUser= Lokasi::all();
            $allUser2= Monitoring::all();
            $view->with(['allUser' => $allUser, 'allUser2' => $allUser2]);
        });
    }
}
